# Parsing des éléments INFO dans les fichiers wav

Le sujet de ce projet consiste à fixer le bug https://code.videolan.org/videolan/vlc/-/issues/6587 ouvert il y a 10 ans.

Il faut supporter les métadonnées qui existent dans les fichier WAV, pour les afficher aux utilisateurs.

## A regarder avant
- le fichier de test: http://streams.videolan.org/issues/6587/TaggedWAV.wav
- la documentation du format Wav: http://www.tactilemedia.com/info/MCI_Control_Info.html et notamment la partie sur `INFO List Chunk`
- le fichier de VLC à modifier: modules/demux/wav.c

## Étape 1: débugger

La première étape consiste à ajouter du débug dans modules/demux/wav.c dans la fonction d'Open grâce à la fonction `msg_Err` et vérifier que cela s'affiche bien dans la console au lancement du fichier avec VLC.

Je vous conseille aussi d'installer un logiciel comme Okteta, pour faire de l'analyse binaire.

## Étape 2: attraper les LIST

On va donc attraper un chunk de type LIST en plus des chunk `DATA`, `ds64`ou `fmt ` qui existent déjà.

Pour cela, il faut:
- ajouter un `wav_chunk_id_list` à `enum wav_chunk_id`
- modifier wav_chunk_id_key_list pour supporter le tag `LIST`
- créer une fonction `ChunkParseLIST` basée sur `ChunkParseDS64`
- ajouter un cas dans le grand switch/case de `Open()`

Dans la fonction `ChunkParseLIST`, il faut simplifier pour garder principalement: 

```msg_Err( p_demux, "'LIST' chunk %i", i_size );```

En théorie, vous devriez trouver une valeur de 100 au runtime, sur le fichier sus-cité.

Si vous avez réussi jusque là, BRAVO!

## Étape 3: rentrer dans le chunk INFO

Maintenant, on doit aller trouver les informations dans le chunk INFO qui est contenu dans le LIST.

Les 4 octets après LIST correspondent à la taille du chunk (100 ici). Il faut donc avancer de 4 octets grâce à
`vlc_stream_Peek( p_demux->s, &p_peek, 4)`

Vous devriez trouver les charactères INFO dans p_peek, et les afficher avec msg_Err.

## Étape 4: trouver les strings de metadonnées

En utilisant `vlc_stream_Peek` et `msg_Err()` vous devriez trouver les bonnes strings IPRD, IART, ICRD, IGNR..

## Étape 5: setter les métadonnées

Utiliser les fonctions de include/vlc_meta.h pour afficher les metadonnées trouvées.

Utiliser le tableau
https://exiftool.org/TagNames/RIFF.html pour en ajouter d'autres

## Étape 6 (optionnel): refaire l'exercice dans VLC 4.

## Étape 7 (optionnel): supporter plus de metadonnées

Utiliser les fichiers dans le 7z
http://streams.videolan.org/issues/6587/WAVs-WithTags.7z pour supporter plus de metadonnées

## Étape 8 (optionnel): nettoyer le code pour préparer la soumission upstream
