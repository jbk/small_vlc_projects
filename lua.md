# Lua scripts & VLC 

VLC peut utiliser des scripts Lua pour lire des video de sites connus (Youtube, Twitch, Soundcloud, etc..) mais aussi pour faire des annuaires de contenus.

La liste des sites supportés est là:
https://code.videolan.org/videolan/vlc/-/tree/master/share/lua/playlist et
https://code.videolan.org/videolan/vlc/-/tree/master/share/lua/sd/


## Étape 1: faire fonctionner les scripts

Lire les scripts simples comme Vimeo et Soundcloud.

Lire la doc https://code.videolan.org/videolan/vlc/-/blob/master/share/lua/playlist/README.txt

et tester les scripts actuels.


## Étape 2: faire un script pour TikTok

Faire un nouveau script lua pour lire les videos depuis TikTok

## Étape 3: faire un script SD pour Podcastindex

Pour avoir un podcast dans VLC, il faudrait utiliser:
https://podcastindex.org/ et faire un SD en lua.

Il faut lire https://code.videolan.org/videolan/vlc/-/tree/master/share/lua/sd/ et le README associé.

## Étape 4: préparer l'upstreaming des scripts lua fabriqués.

Il faut nettoyer son code, préparer VLC 4.0, et préparer les scripts pour être ajoutés dans VLC.

## Étape 5 (optionnel): réparer les scripts ou en écrire de nouveaux    

On peut penser à réparer les autres scripts ou en écrire de nouveaux, autre que TikTok :)

