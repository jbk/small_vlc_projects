# VLC micro projects

Bienvenus !

Aujourd'hui, nous allons travailler sur des mini-projets pour VLC.

Il y a 3 sujets :
 - un projet de parsing de fichier Wav, qui pourra être mergé à la fin, dans VLC.
 - un projet qui porte sur les filtres vidéos, notamment, blur et zoom.
 - un projet pour le support des videos TikTok dans VLC en lua, ainsi qu'un répertoire de podcast.

Les 2 premiers sujets sont à faire en C, et le dernier en lua.

## Étape 0: Pré-ambule

Le préambule de tous ces projets, consiste à compiler VLC.

Compiler VLC n'est pas une tâche facile. Il est normal que cela prenne du temps.

### Debian, Ubuntu

Il est fortement recommandé d'utiliser Debian, Ubuntu pour compiler. Une version récente est nécessaire!

Compiler VLC sous macOS est très compliqué, et Windows encore plus...

### VLC 3.0

Pour compiler:
- Git cloner https://code.videolan.org/videolan/vlc.git
- Changer de branch pour la version `3.0.x`
- vérifier que tout le code a été checkout

Ensuite, il faut:
- bootstrapper VLC
- configurer VLC
- compiler VLC

Pour ce faire, il est recommandé de lire https://wiki.videolan.org/UnixCompile/ et de suivre les instructions.
*bien penser à faire apt-get build-dep vlc*

**NB: NE PAS faire make install**

Lancer VLC avec `./vlc -vv`

Vérifier que vous pouvez lire un fichier vidéo normal.

### Optionnel: VLC 4.0

Si compiler VLC 3.0 a été rapide (moins de 3h), je vous conseille de compiler VLC 4.0.

C'est plus compliqué, car il faut beaucoup plus de dépendances que la 3.0, et `apt-get build-dep vlc` ne va pas vous aider suffisamment.

Cependant, c'est un exercice plus intéressant.

Je vous conseille de configurer  avec `./configure --disable-medialibrary`

## Etape 1: modifier VLC

La première vraie étape consiste à modifier l'interface de VLC pour afficher votre nom dedans.

Le code à modifier se situe dans `modules/gui/qt/` il faut un peu chercher pour trouver le bon bout :)

## Etape 2: choisir un sujet

Le premier sujet, parle de parsing de fichier Wav en C, un sujet important, mais qui peut être difficile au premier abord.
De plus, à la fin de ce sujet, il sera possible de proposer cette modification à l'équipe de VLC, car c'est une fonctionnalité qui manque. [Sujet WAV](Wav.md).

Le second sujet parle de travailler sur des filtres vidéos en C. [Sujet Filtre](video_filter.md).

Le dernier sujet, un peu plus simple, consiste à ajouter le support des vidéos tiktok dans VLC, en lua. [Sujet Lua](lua.md).

**NB: il est totalement possible de faire les 3 sujets pendant le temps imparti.**

*Gambatte Kudasai*
