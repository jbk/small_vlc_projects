# Filtres vidéos

Ce projet va vous faire rentrer dans les filtres vidéos.

On va principalement travailler sur les filtres de blur et sur le filtre de zoom, sur un VLC 3.0.

## Étape 1: utiliser le filtre de gaussianblur

Utiliser VLC avec un fichier video et le filtre gaussianblur et un sigma de 10 en ligne de commande.

Pensez à désactiver le décodage matériel.

*(oui, ce filtre est lent, ce n'est pas important)*

## Étape 2: restreindre le filtre à une région.

Maintenant, il faut limiter ce blur à une région rectangulaire, avec 4 options (x, y, hauteur, largeur), pour ne faire le blur que sur cette région et pas ailleurs.

## Étape 3: noircir la zone

Faire la même manipulation, mais remplissez de noir le cœur de cette region, au lieu de la blur.

Bravo!

## Étape 4: modifier le zoom interactif pour accepter le RGB

On veut maintenant lire des PNG et pouvoir zoomer dessus.

Il faut utiliser donc le filtre magnify, qui s'active avec `vlc -vvv --video-filter magnify` pour zoomer dans VLC.

Mais il ne supporte aujourd'hui que le YUV, mais le mode RGB. Donc, si on ouvre un PNG, le filtre ne se charge pas.

Il faut changer le filtre Create pour accepter le format RV32 ou RGBA dans le switch/case.

## Étape 5: filtrer correctement en RGB

### Comprendre les plans de video

Lire https://wiki.videolan.org/YUV et regarder https://en.wikipedia.org/wiki/YUV et le début de https://github.com/leandromoreira/digital_video_introduction 

### En pratique

En RV32, on a un seul plan d'image au lieu de plusieurs en YUV (3 en général), il faut donc modifier le travail sur les planes au début de la fonction Filter (après `if( o_zoom != ZOOM_FACTOR )`)

Normalement, il faut simplifier ce code, car la magie opère plus bas, avec l'appel à `image_Convert`

## Étape 6 (optionnel): préparer le code pour upstream

On vient donc de fixer le bug https://code.videolan.org/videolan/vlc/-/issues/16466

Il faut maintenant nettoyer ce code, le préparer pour la 4.0 et préparer un patch pour l'envoyer upstream.

Good luck!  



